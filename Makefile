## ECWID box

.DEFAULT_GOAL := help
.PHONY:
SHELL=/bin/bash

EMAIL=$(shell git config user.email)
export

help: ## get help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'

.PHONY: build
build: down ## Build solution docker image
	docker-compose build --no-cache

.PHONY: up
up: build ## Up docker container
	docker-compose up -d

.PHONY: status
status:
	docker-compose ps

.PHONY: start
start:
	docker-compose start

.PHONY: stop
stop:
	docker-compose stop

.PHONY: down
down:
	docker-compose down -v

.PHONY: attach
attach:
	docker-compose exec box bash

.PHONY: logs
logs:
	docker-compose logs box
