#!/bin/bash

### BEGIN INIT INFO
# Provides:             box
# Required-Start:       $local_fs $network $time
# Required-Stop:        $local_fs $network $time
# Should-Start:         $syslog
# Should-Stop:          $syslog
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    Part of the test task for Ecwid ops engineer
### END INIT INFO

DAEMON=/opt/box/bin/box
DAEMON_ARGS='/etc/box.properties'
DESC='Ecwid ops test task'
# JAVA_OPTS='-Xmx50m'
NAME=box
PATH=/sbin:/usr/sbin:/bin:/usr/bin
PIDFILE=/run/box.pid
SCRIPTNAME=/etc/init.d/box

[ -r /etc/default/box ] && . /etc/default/box

[ -x "$DAEMON" ] || exit 0

. /lib/init/vars.sh
. /lib/lsb/init-functions

do_start() {

    STATUS=$(sudo -u postgres pg_isready | grep 'no response')
    while [[ $STATUS == 0 ]]; do
        echo "psql is down: $STATUS"
        sleep 1
    done

    sh /db_init_scripts/provisioning.sh

    export JAVA_OPTS
    # Return
    #   0 if daemon has been started
    #   1 if daemon was already running
    #   2 if daemon could not be started
    start-stop-daemon --start --pidfile $PIDFILE --startas $DAEMON \
        --test >/dev/null ||
        return 1
    start-stop-daemon --start --pidfile $PIDFILE --startas $DAEMON \
        --background --make-pidfile -- $DAEMON_ARGS ||
        return 2
}

do_stop() {
    # Return
    #   0 if daemon has been stopped
    #   1 if daemon was already stopped
    #   2 if daemon could not be stopped
    #   other if a failure occurred
    start-stop-daemon --stop --pidfile $PIDFILE --remove-pidfile \
        --retry=TERM/30/KILL/5
    RETVAL="$?"
    [ "$RETVAL" = 2 ] && return 2
    # Wait for children to finish too if this is a daemon that forks
    # and if the daemon is only ever run from this initscript.
    # If the above conditions are not satisfied then add some other code
    # that waits for the process to drop all resources that could be
    # needed by services started subsequently.  A last resort is to
    # sleep for some time.
    start-stop-daemon --stop --pidfile $PIDFILE --remove-pidfile \
        --retry=0/30/KILL/5 --quiet --oknodo
    [ "$?" = 2 ] && return 2
    return "$RETVAL"
}

case "$1" in
start)
    log_daemon_msg "Starting $DESC" "$NAME"
    do_start
    case "$?" in
    0 | 1) log_end_msg 0 ;;
    2) log_end_msg 1 ;;
    esac
    ;;
stop)
    log_daemon_msg "Stopping $DESC" "$NAME"
    do_stop
    case "$?" in
    0 | 1) log_end_msg 0 ;;
    2) log_end_msg 1 ;;
    esac
    ;;
status)
    status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
    ;;
restart | force-reload)
    log_daemon_msg "Restarting $DESC" "$NAME"
    do_stop
    case "$?" in
    0 | 1)
        do_start
        case "$?" in
        0) log_end_msg 0 ;;
        1) log_end_msg 1 ;; # Old process is still running
        *) log_end_msg 1 ;; # Failed to start
        esac
        ;;
    *)
        # Failed to stop
        log_end_msg 1
        ;;
    esac
    ;;
*)
    echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
    exit 3
    ;;
esac
