#!/bin/bash
MY_PATH=$(dirname "$0")

set -e

if [ ! -f ./init_complete ]; then
    echo "Get password" 2>&1 >>${MY_PATH}/provision.log
    PASSWORD=$(cat /etc/box.properties | grep dbPass | cut -d'=' -f2 | xargs)
    echo "Run db provisioning" 2>&1 >>${MY_PATH}/provision.log
    echo $(sudo -u postgres pg_isready) 2>&1 >>${MY_PATH}/provision.log
    echo $? 2>&1 >>${MY_PATH}/provision.log
    sudo -u postgres psql -f ${MY_PATH}/provisioning.sql -v passwd="'${PASSWORD}'" 2>&1 >>${MY_PATH}/provision.log &&
        sudo -u postgres psql -d box -f ${MY_PATH}/provisioning_tables.sql -v email="'${EMAIL:-test@mail.som}'" 2>&1 >>${MY_PATH}/provision.log &&
        touch ${MY_PATH}/init_complete
    echo "Cpmplete!" 2>&1 >>${MY_PATH}/provision.log
fi
