FROM ecwid/ops-test-task:20210311a
ARG EMAIL
# 1. Fix nginx
COPY solution/nginx/box.conf /etc/nginx/sites-available/box.conf

# 2. Fix box
# Not working see: vi /etc/init.d/box
# not working logging properties and JAVA_OPTS
# need overwrite starter box script to avoid JAVA_OPTS problems
# ENV JAVA_OPTS="-Xmx512m"
# not working and not reccomend becouse this settings will be owerrite all default JVM bindings
#COPY solution/box/fusion/java.options /etc/opt/fusion/java.options
# Need replace JAVA_OPTS with sed command
# COPY solution/box/init_box.sh /etc/init.d/box

# 3. Fix db an add provisioning
COPY solution/db /db_init_scripts
RUN echo "host    all             box             127.0.0.1/32            md5" >> /etc/postgresql/12/main/pg_hba.conf && \
    sed -i -r 's/host\s+all\s+all\s+127\.0\.0\.1\/32\s+md5//' /etc/postgresql/12/main/pg_hba.conf
RUN service postgresql start && sh /db_init_scripts/provisioning.sh ${EMAIL} && service postgresql stop

# COPY solution/startup.sh /startup