#! /usr/bin/bash

set -e

service postgresql start
service box start
service nginx start

echo >&2 "All services started, looping forever. Press ^C to interrupt and terminate."

trap 'exit 130' INT

function check_version() {
    local suggested=$(
        curl -s https://hub.docker.com/v2/repositories/ecwid/ops-test-task/ |
            jq .full_description -r |
            grep -Eo 'ecwid/ops-test-task:[0-9a]+' |
            cut -d: -f2
    )
    local current=$(cat /version)
    if [ -n "$suggested" -a "$suggested" != "$current" ]; then
        cat <<____

В тексте задания (https://ecwid.to/ops) указана версия контейнера "$suggested",
а у тебя запущена "$current".  Вероятно, в ней нашёлся какой-то баг, поэтому
мы выпустили исправление.  Лучше бы тебе взять версию "$suggested".

____
    fi
}

while true; do
    for m in {1..60}; do
        check_version >&2
        echo -n . >&2
        for s in {1..60}; do
            sleep 1
        done
    done
    date +%Hh >&2
done
